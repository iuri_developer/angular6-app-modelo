# angular6-app-modelo

A estrutura da aplicação no angular é dividida da seguinte forma:

- _angular.json_
- src
    - app
        - config
        - guards
        - helpers
        - interceptors
        - models
        - services
        - components
        - lib
        - shared
        - _app.component.html_
        - _app.component.ts_
        - _app.module.ts_
        - _app.routing.ts_
    - assets
        - css
        - images
    - _index.html_
    - _main.ts_
    
### A documentação abaixo tem como propósito deixar claro as principais responsabilidades dentro da arquitetura acima.

#### Arquivo _angular.json_
Este arquivo contém as principais configurações do aplicativo. Uma configuração que podemos utilizar de exemplo é, qual o arquivo html será o index do app ou qual o arquivo typescript será o arquivo de configuração de amabiente.

#### app/_config
Os arquivos de configuração terão a função de centralizar informações de configuração da aplicação. Um bom exemplo de arquivo de configuração é o "environment.ts", que é um arquivo do tipo TypeScript que centraliza variáveis que podem ser utilizadas em diversas classes da aplicação, diminuindo a necessidade de refatoração no código quando uma única informação precisa está em diferentes pontos do código.

#### app/_guards
Os arquivos de guarda, tem como objetivo realizar configurações de segurança nas rotas da aplicação. Existem alguns tipos de guarda, dentre eles:
- CanActivate: valida se a rota está ativa;
- CanActivateChild: valida se a rota filha está ativa;
- CanDeactivate: verifica se uma rota pode ser desativada;
- CanLoad: valida se um módulo está utilizando o lazy loading.

Para a aplicação de planejamento, iniciamos utilizando o guarda CanActivate. Onde dentro do arquivo "auth.guard.ts" é feita uma validação que, caso o token da aplicação não exista no localstorage ou, o token tenha expirado, o usuário será redirecionado para a rota inicial do sistema.

#### app/_helpers
TODO

#### app/_interceptors
Como o próprio nome já diz, os interceptors servem para interceptar requisições http ($http.get, $http.post, etc..) request do angular. Geralmente são utilizados para atender os cenários abaixo: 
- Colocar um header em todas as requisições que serão enviadas ao backend;
- Ter um tratamento genérico para todas as resposta do backend (ex.: erro 404, erro 500 );
- Iniciar e parar uma spinner de forma automática a cada requisição ao backend;


#### app/_models
TODO

#### app/services
As services são abrangentes e incluem qualquer valor, função ou recurso de que um aplicativo precisa. Uma service é tipicamente uma classe com um propósito estreito e bem definido. Deve fazer algo específico e fazê-lo bem. Geralmente, é o principal responsável por se comunicar com o server-side e devolver a resposta para o componente de interface de usuário.

#### app/components
Um componente é tecnicamente uma diretiva. No entanto, os componentes são tão distintos e centrais para aplicativos Angulares que o Angular define o decorador @Component ().

Tipos de componentes do angular:

- {fileName}.component.ts - componente que compara-se à controller do mvc, é com esta classe que a view irá se comunicar diretamente;
- {fileName}.spec.component.ts - componente de especificação de testes de unidade.
- {fileName}.style.component.css - componente de estilos;
- {fileName}.view.component.html - componente de visualização.

#### lib
TODO
#### shared
TODO

### Modules

Módulo é a definição do aplicativo. Por padrão, a CLI do angular cria um módulo para cada assunto tratado. 

Neste projeto, arquiteturamos de uma maneira diferente. Além do módulo principal, para cada componente que se faça necessário criar, a CLI do angular cria um sub módulo para implementação deste componente.

Com o intuito de simplificar isso, criamos um módulo principal chamado _app.module.ts_. Este módulo, injeta um arquivo de configuração de rotas chamado _app.routing.ts_ que deve conter a configuração de todas as rotas do app.
O arquivo _app.module.ts_ é o manda chuva. Nele devem estar contidos todas as declarações de componentes, demais módulos e classes de serviços.


